create table message (
    id bigint not null,
    user_id bigint,
    file_name varchar(255),
    tag varchar(255),
    title varchar(2048),
    primary key (id)) engine=InnoDB;

create table user_role (
    user_id bigint not null,
    roles enum ('ADMIN','USER')) engine=InnoDB;

create table users (
    active bit not null,
    id bigint not null,
    activation_code varchar(255),
    email varchar(255),
    password varchar(255),
    username varchar(255),
    primary key (id)) engine=InnoDB;

alter table message
    add constraint FKpdrb79dg3bgym7pydlf9k3p1n
        foreign key (user_id)
            references users (id);

alter table user_role
    add constraint FKj345gk1bovqvfame88rcx7yyx
        foreign key (user_id)
            references users (id);