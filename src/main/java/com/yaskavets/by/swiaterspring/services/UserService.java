package com.yaskavets.by.swiaterspring.services;

import com.yaskavets.by.swiaterspring.enums.Role;
import com.yaskavets.by.swiaterspring.models.User;
import com.yaskavets.by.swiaterspring.repositories.UserRepositories;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.thymeleaf.util.StringUtils;

import java.security.Principal;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepositories repos;

    private final PasswordEncoder passwordEncoder;

    private final CustomMailSender mailSender;


    public List<User> userList ()
    {
        return repos.findAll();
    }
    public boolean userAdd(User user)
    {
        if(repos.findByUsername(user.getUsername())!=null) return false;
        else
        {
            user.getRoles().add(Role.USER);
            user.setActivationCode(UUID.randomUUID().toString());
            user.setPassword(passwordEncoder.encode(user.getPassword()));
            repos.save(user);

            sendMessage(user);
        }
        return true;
    }

    private void sendMessage(User user) {
        if(!StringUtils.isEmpty(user.getEmail()))
        {
            String mesagge = String.format("Hello, %s \n" +
                    "Welcome to Sweater, Please, visit to next link: http://localhost:8080/user/registration/activate/%s", user.getUsername(), user.getActivationCode());
            mailSender.send(user.getEmail(), "Activation code", mesagge);
        }
    }


    public User getAuthor(Principal principal) {
        if(principal==null)
        {
            return new User();
        }
        return repos.findByUsername(principal.getName());

    }

    public boolean activateUser(String code) {

        User user = repos.findByActivationCode(code);
        System.out.println(user);
        if (user==null) return false;
        user.setActivationCode(null);
        user.setActive(true);
        repos.save(user);
        return true;

    }

    public void removeUser(Long id) {
        repos.deleteById(id);
    }

    public boolean saveEditUser(Long id, User user) {

        User findUser = repos.findById(id).orElse(null);
        if (findUser != null) {
            findUser.setUsername(user.getUsername());
            findUser.setRoles(user.getRoles());
            repos.save(findUser);
            return true;
        }
        return false;
    }

    public void updateProfile(User user, String password, String email) {
        String userEmail = user.getEmail();
        boolean isEmailChanged = (email != null && !email.equals(userEmail)) ||
                (userEmail != null && !userEmail.equals(email));
        if (isEmailChanged) {
            user.setEmail(email);
            if (!StringUtils.isEmpty(email)) {
                user.setActivationCode(UUID.randomUUID().toString());

            }
        }
        if(StringUtils.isEmpty(password)) user.setPassword(password);
        repos.save(user);
        if(isEmailChanged) sendMessage(user);

    }

}
