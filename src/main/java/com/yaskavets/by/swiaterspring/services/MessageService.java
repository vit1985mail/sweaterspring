package com.yaskavets.by.swiaterspring.services;

import com.yaskavets.by.swiaterspring.models.Message;
import com.yaskavets.by.swiaterspring.repositories.MessageRepositories;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.awt.*;
import java.io.File;
import java.security.Principal;
import java.util.ArrayList;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class MessageService {

    @Value("${upload.path}")
    private String uploadPatch;
 private final MessageRepositories repos;
 private final UserService userService;

 @SneakyThrows
 public void messageAdd(Message message, Principal principal, MultipartFile file)
 {
     message.setAuthor(userService.getAuthor(principal));
     if(file.getSize()!=0&& !file.getOriginalFilename().isEmpty())
     {
         File uploadDir = new File(uploadPatch);
         if(!uploadDir.exists())
         {
             uploadDir.mkdir();
         }
         String uuidFile = UUID.randomUUID().toString();
         String resultFileName = uuidFile + "." + file.getOriginalFilename();
         file.transferTo(new File(uploadPatch + "\\" + resultFileName));
         message.setFileName(resultFileName);
     }
     repos.save(message);
    }

    public ArrayList<Message> getMessagList() {
     return (ArrayList<Message>) repos.findAll();
    }

    public void deleteMessage(Long id) {
     repos.deleteById(id);
    }

    public ArrayList<Message> findListToTag(String tag)
    {
        System.out.println(repos.findByTag(tag));
        return (ArrayList<Message>)(!repos.findByTag(tag).isEmpty()?repos.findByTag(tag):repos.findAll());
    }
}
