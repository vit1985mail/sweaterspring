package com.yaskavets.by.swiaterspring.configs;

import com.yaskavets.by.swiaterspring.models.User;
import com.yaskavets.by.swiaterspring.models.UserNotActivatedException;
import com.yaskavets.by.swiaterspring.repositories.UserRepositories;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.ui.Model;

@Configuration
@EnableWebSecurity
@EnableMethodSecurity
@RequiredArgsConstructor
public class SecurityConfig {

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception
    {
        http
                .authorizeHttpRequests((requests)->requests
                        .requestMatchers("/user/registration/**", "/greeting").permitAll()
                        .anyRequest().authenticated()
                )
                .formLogin((form)->form
                        .loginPage("/login")
                        .permitAll()
                )
                .logout((logout)->logout.permitAll());
        return http.build();
    }

    @Bean
    @SneakyThrows
    public UserDetailsService userDetailsService(UserRepositories urepos)
    {
        return username -> {
            User user = urepos.findByUsername(username);
            if(user!=null)
            {
                if(user.isActive()){
                return user;}
                else
                {
                    try {
                        throw new UserNotActivatedException("Пользователь  " + username + " не активирован, проверьте почту");
                    } catch (UserNotActivatedException e) {
                        throw new RuntimeException(e);
                    }
                }
            }
            else
            {
                throw new UsernameNotFoundException("Пользователь  " + username + " не найден");
            }
        };
    }

    @Bean
    public PasswordEncoder passwordEncoder()
    {
        return new BCryptPasswordEncoder(8);
    }


}
