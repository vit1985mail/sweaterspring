package com.yaskavets.by.swiaterspring.models;

public class UserNotActivatedException extends Exception {
    public UserNotActivatedException(String messagge) {
        super(messagge);
    }
}
