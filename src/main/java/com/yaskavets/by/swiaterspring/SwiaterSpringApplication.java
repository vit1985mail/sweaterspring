package com.yaskavets.by.swiaterspring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class  SwiaterSpringApplication {

    public static void main(String[] args) {
        SpringApplication.run(SwiaterSpringApplication.class, args);
    }

}
