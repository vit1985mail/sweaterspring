package com.yaskavets.by.swiaterspring.repositories;

import com.yaskavets.by.swiaterspring.models.Message;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MessageRepositories extends JpaRepository<Message, Long> {
    List<Message> findByTag(String tag);
}
