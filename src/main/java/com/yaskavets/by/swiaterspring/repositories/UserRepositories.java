package com.yaskavets.by.swiaterspring.repositories;

import com.yaskavets.by.swiaterspring.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepositories extends JpaRepository<User, Long> {
    User findByUsername(String username);

    User findByActivationCode(String code);
}
