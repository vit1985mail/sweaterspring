package com.yaskavets.by.swiaterspring.controllers;

import com.yaskavets.by.swiaterspring.enums.Role;
import com.yaskavets.by.swiaterspring.models.User;
import com.yaskavets.by.swiaterspring.models.dto.CaptchaResponseDto;
import com.yaskavets.by.swiaterspring.services.UserService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.Map;


@Controller
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserController {

    private final static String CAPTCHA_URL="https://www.google.com/recaptcha/api/siteverify?secret=%s&response=%s";

    @Value("${recaptcha.secret}")
    private String secret;

    @Autowired
    private final UserService userService;

    @Autowired
    private RestTemplate restTemplate;



    @GetMapping("/registration")
    public String register()
    {
        return "registration";
    }

    @GetMapping
    @PreAuthorize("hasAuthority('ADMIN')")
    public String userList(Model model)
    {
        model.addAttribute("userList", userService.userList());
        return "userlist";
    }

    @PostMapping("/registration")
    public String userAddToData(@RequestParam("password2") String password2,
                                @RequestParam ("g-recaptcha-response") String captchaResponse,
                                @Valid User user,
                                BindingResult bindingResult, Model model)
    {
        String url = String.format(CAPTCHA_URL, secret, captchaResponse);
        CaptchaResponseDto response = restTemplate.postForObject(url, Collections.emptyList(), CaptchaResponseDto.class);
        if(!response.isSuccess())
        {

            model.addAttribute("captchaError", "Fill captcha");
        }
        boolean isConfirmEmpty = password2.equals("");
        if (isConfirmEmpty) {
            model.addAttribute("password2Error", "Password confirmation cannot be empty");
        }
        if (user.getPassword() != null &&!password2.equals(user.getPassword())) {
            model.addAttribute("passwordError", "Пароли не совпадают");
        }

        if(isConfirmEmpty||bindingResult.hasErrors()||!response.isSuccess())
        {
            Map<String, String> errorsMap = ControllerUtils.getErrors(bindingResult);
            model.mergeAttributes(errorsMap);
            model.addAttribute("user", user);
            return "registration";
        }

        if(!userService.userAdd(user))
        {
            model.addAttribute("errorMessage", "Пользователь с именем: " + user.getUsername() +
                    " уже существует");
            return "registration";
        }

        return "redirect:/login";
    }
    @PostMapping("/delete/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public String removeUser(@PathVariable Long id)
    {
        userService.removeUser(id);
        return "redirect:/user";
    }

    @GetMapping("/registration/activate/{code}")
    public String activate (@PathVariable String code, Model model)
    {
        boolean isActivated = userService.activateUser(code);
        if(isActivated)
        {
            model.addAttribute("errorMessageSuccess", "Пользователь успешно активирован");
        }
        else {

            model.addAttribute("errorMessageAnSuccess", "Активация пользователя не проведена");
        }
        return "login";
    }
    @GetMapping("/{user}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public String editUser(@PathVariable User user, Model model)
    {
        model.addAttribute("user", user);
        model.addAttribute("rols", Role.values());
        return "editUserForm";
    }
    @PostMapping()
    public String userEditSave(@RequestParam ("idUser") Long id, User user, Model model)
    {
        if(userService.saveEditUser(id, user)) return "redirect:/user";
        model.addAttribute("user", user);
        model.addAttribute("rols", Role.values());
        return "editUserForm";
    }
    @GetMapping("profile")
    public String updateProfile(Model model, @AuthenticationPrincipal User user)
    {
        model.addAttribute("username", user.getUsername());
        model.addAttribute("email", user.getEmail());
        return "profile";
    }
    @PostMapping("profile")
    public String updateProfile (@AuthenticationPrincipal User user, @RequestParam String password, @RequestParam String email)
    {

        userService.updateProfile(user, password, email);

        return "redirect:/user/profile";
    }
}
