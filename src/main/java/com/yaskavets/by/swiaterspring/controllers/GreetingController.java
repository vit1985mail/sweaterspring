package com.yaskavets.by.swiaterspring.controllers;


import com.yaskavets.by.swiaterspring.models.Message;
import com.yaskavets.by.swiaterspring.services.MessageService;
import com.yaskavets.by.swiaterspring.services.UserService;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.security.Principal;
import java.util.Map;

@Controller
@RequiredArgsConstructor
public class GreetingController {

    private final MessageService mService;
    private final UserService uServis;

    @GetMapping("/login")
    public String login()
    {
        return "login";
    }

    @GetMapping("/greeting")
    public String greeting(@RequestParam (name = "name", required = false, defaultValue = "World") String name, Model model)
    {
        model.addAttribute("name", name);
        return "greeting";
    }

    @GetMapping
    public String listMessage(@RequestParam (name="searchTag", required = false) String title, Principal principal, Model model)
    {
        if(title!=null&&!title.isEmpty())
        {
            model.addAttribute("messages", mService.findListToTag(title));
            model.addAttribute("title", title);
        }
        else {
            model.addAttribute("messages", mService.getMessagList());
        }
        model.addAttribute("user", uServis.getAuthor(principal));
        return "listMessage";
    }
    @PostMapping("/creat/messageAdd")
      public String messageAdd(Principal principal, @Valid Message message, BindingResult bindingResult, Model model, @RequestParam("file") MultipartFile file)
    {

        if(bindingResult.hasErrors())
        {
            Map<String, String> errorsMap = ControllerUtils.getErrors(bindingResult);
            model.mergeAttributes(errorsMap);
            model.addAttribute("message", message);
            model.addAttribute("user", uServis.getAuthor(principal));
            return "listMessage";
        }
        else {
            mService.messageAdd(message, principal, file);

            return "redirect:/";
        }
    }

    @PostMapping("/delete/{id}")
    public String removeMessage(@PathVariable("id") Long id)
    {
        mService.deleteMessage(id);
        return "redirect:/";
    }


}
